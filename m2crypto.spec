Name:          m2crypto
Summary:       A crypto and SSL toolkit for Python
Version:       0.39.0
Release:       1
License:       MIT
URL:           https://gitlab.com/m2crypto/m2crypto/
Source0:       %{pypi_source M2Crypto}

BuildRequires: openssl openssl-devel perl-interpreter pkgconfig  swig which
BuildRequires: python3-devel python3-setuptools gcc

%description
M2Crypto is a crypto and SSL toolkit for Python. It allows you to call OpenSSL functions from Python2 scripts.

%package       -n python3-m2crypto
Summary:       A crypto and SSL toolkit for Python3

%description   -n python3-m2crypto
python3-m2crypto is a crypto and SSL toolkit for Python3. It allows you to call OpenSSL functions from Python3 scripts.

%prep
%autosetup -n M2Crypto-%{version} -c -p1

cp -a M2Crypto-%{version} M2Crypto-python3

%build
export CFLAGS="$RPM_OPT_FLAGS"
if pkg-config openssl ; then
    export CFLAGS="$CFLAGS `pkg-config --cflags openssl`"
    export LDFLAGS="$LDFLAGS`pkg-config --libs-only-L openssl`"
fi

cd M2Crypto-%{version}
%{__python3} setup.py build


%install
cd M2Crypto-%{version}
%{__python3} setup.py install --root=$RPM_BUILD_ROOT


%check
%if 0%{?_with_check}
cd M2Crypto-%{version}
%{__python3} setup.py test
%endif

%files -n python3-m2crypto
%doc M2Crypto-python3/{CHANGES,LICENCE,README.rst}
%{python3_sitearch}/M2Crypto
%{python3_sitearch}/M2Crypto-*.egg-info

%changelog
* Sat Jul 22 2023 xu_ping <707078654@qq.com> - 0.39.0-1
- Upgrade to 0.39.0

* Wed Aug 17 2022 yaoxin <yaoxin30@h-partners.com> - 0.38.0-2
- Fix CVE-2020-25657

* Thu Apr 28 2022 houyingchao <houyingchao@h-partners.com> - 0.38.0-1
- Upgrade to 0.38.0

* Wed Jun 23 2021 wuchaochao <wuchaochao4@huawei.com> - 0.30.1-5
- add buildrequires:gcc

* Thu Oct 29 2020 tianwei <tianwei12@huawei.com> - 0.30.1-4
- delete python2

* Wed Nov 06 2019 Lijin Yang <yanglijin@huawei.com> - 0.30.1-3
- init package
